import _ from 'lodash'

import { useState } from "react";
import Cell from "./Cell";

function Board(props) {
    const [grid, setGrid] = useState([["","",""],["","",""],["","",""]])

    function handlePlayAgain(event){
        setGrid([["","",""],["","",""],["","",""]]);
    }

    function handleClick(event){
        const col = event.target.getAttribute("col");
        const row = event.target.getAttribute("row");

        // if position is already X or O || if game is already over
        // don't allow more clicking by "return"
        if ((grid[row][col] !== "") || (checkEndGame(grid, false))) {
            return
        }

        const newGrid = _.cloneDeep(grid);
        newGrid[row][col] = getTurn();
        setGrid(newGrid);

        checkEndGame(newGrid, true);


    }

    function getTurn(){
        let xCount = 0;
        let oCount = 0;
        for (let row = 0; row < grid.length; row++) {
            for (let col = 0; col < grid[0].length; col++) {
                let val = grid[row][col]
                if (val === "X"){
                    xCount += 1;
                }
                if (val === "O"){
                    oCount += 1;
                }
            }
        }
        return xCount === oCount ? "X" : "O";
    }

    function resetBoard(){
        setGrid([["","",""],["","",""],["","",""]]);
    }

    function checkEndGame(newGrid, shouldUpdateScore){
        if (checkWin(newGrid, "X")){
            if (shouldUpdateScore){
                props.increaseResultCount("X");
            }
            return true;
        }
        if (checkWin(newGrid, "O")){
            if (shouldUpdateScore){
                props.increaseResultCount("O");
            }
            return true;
        }
        if (checkTie(newGrid)){
            if (shouldUpdateScore){
                props.increaseResultCount("Tie");
            }
            return true;
        }
        return false;
    }

    function checkWin(newGrid, value){
        if (
            checkRowWin(newGrid, value) ||
            checkColWin(newGrid, value) ||
            checkDownDiagonalWin(newGrid, value) ||
            checkUpDiagonalWin(newGrid, value)
        ) {
            return true;
        };

    }

    function checkRowWin(newGrid, value){
        for (let row = 0; row < newGrid.length; row++) {
            let rowWin = true;
            for (let col = 0; col < newGrid[0].length; col++) {
                const val = newGrid[row][col]
                if (val !== value){
                    rowWin = false;
                }
            }

            if (rowWin === true){
                return true;
            }
        }
    }

    function checkColWin(newGrid, value){
        for (let col = 0; col < newGrid[0].length; col++){
            let colWin = true;
            for (let row = 0; row < newGrid.length; row++){
                const val = newGrid[row][col]
                if (val !== value){
                    colWin = false;
                }
            }

            if (colWin === true){
                return true;
            }
        }
    }

    function checkDownDiagonalWin(newGrid, value){
        for (let row = 0; row < newGrid.length; row++){
            for (let col = 0; col < newGrid[0].length; col++){
                const val = newGrid[row][col]
                if (row === col && val !== value){
                    return false;
                }
            }
        }
        return true;
    }

    function checkUpDiagonalWin(newGrid, value){
        const targetSum = newGrid.length - 1
        for (let row = 0; row < newGrid.length; row++){
            for (let col = 0; col < newGrid[0].length; col++){
                const val = newGrid[row][col]
                if (row + col == targetSum && val !== value){
                    return false;
                }
            }
        }
        return true;
    }

    function checkTie(newGrid){
        for (let row = 0; row < newGrid.length; row++) {
            for (let col = 0; col < newGrid[0].length; col++) {
                const val = newGrid[row][col]
                if (val === ""){
                    return false;
                }
            }
        }
        return true;
    }

    const playAgainButton = <div className="row" style={{ marginTop: '50px' }}>
        <div className="col">
            <button onClick={handlePlayAgain} type="button" className="btn btn-outline-info btn-sm">
                Play Again!
            </button>
        </div>
    </div>

    return (
        <>
            <div className="row justify-content-md-center">
                <Cell row={0} col={0} click={handleClick} val={grid[0][0]} pos={1}/>
                <Cell row={0} col={1}click={handleClick} val={grid[0][1]} pos={2}/>
                <Cell row={0} col={2} click={handleClick} val={grid[0][2]} pos={3}/>
            </div>
            <div className="row justify-content-md-center">
                <Cell row={1} col={0} click={handleClick} val={grid[1][0]} pos={4}/>
                <Cell row={1} col={1} click={handleClick} val={grid[1][1]} pos={5}/>
                <Cell row={1} col={2} click={handleClick} val={grid[1][2]} pos={6}/>
            </div>
            <div className="row justify-content-md-center">
                <Cell row={2} col={0} click={handleClick} val={grid[2][0]} pos={7}/>
                <Cell row={2} col={1} click={handleClick} val={grid[2][1]} pos={8} />
                <Cell row={2} col={2} click={handleClick} val={grid[2][2]} pos={9} />
            </div>
            {checkEndGame(grid, false) && playAgainButton}
        </>
    );
}

export default Board;
