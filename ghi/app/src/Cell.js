import { useState } from "react";

function Cell(props) {
    const style = {height: "30px"};

    if (props.pos <= 6) {
        style.borderBottom = '3px solid black';
    }

    if (props.pos % 3 !== 0) {
        style.borderRight = '3px solid black';
    }


    return (
        <div onClick={props.click} row={props.row} col={props.col} className="col-2" style={style}>
            {props.val}
        </div>
    );
}

export default Cell;
