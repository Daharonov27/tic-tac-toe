import { useState } from "react";
import Board from "./Board";

function Game() {
    const [numXWins, setNumXWins] = useState(0);
    const [numOWins, setNumOWins] = useState(0);
    const [numTies, setNumTies] = useState(0);

    function increaseResultCount(value){
        if (value === "X"){
            setNumXWins(numXWins + 1);
        }

        if (value === "O"){
            setNumOWins(numOWins + 1);
        }

        if (value === "Tie"){
            setNumTies(numTies + 1);
        }
    }

    return (
        <div className="container text-center">
            <div className="row">
                <div className="col">
                    X: {numXWins}
                </div>
                <div className="col">
                    Ties: {numTies}
                </div>
                <div className="col">
                    O: {numOWins}
                </div>
            </div>
            <div className="row" style={{ marginTop: '100px' }}>
                <Board increaseResultCount={increaseResultCount}/>
            </div>
        </div>
    );
}

export default Game;
